import Education from './Education';
//TODO: class not in use
export default class Person {
  constructor(name, age, description) {
    //TODO: better to use _name as private filed
    this.name = name;
    this.age = age;
    this.description = description;
    this.educations = new Education();
  }
}
